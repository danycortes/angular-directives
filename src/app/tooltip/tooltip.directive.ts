import { Directive, Input, ElementRef, ViewContainerRef, OnInit } from '@angular/core';

@Directive({
    selector: '[tooltip]',
    exportAs: 'tooltip'
})

export class TooltipDirective implements OnInit {
    tooltipElement = document.createElement('div');
    visible = false;

    @Input()
    set tooltip( value ) {
        this.tooltipElement.textContent = value;
    }

    hide() {
        this.tooltipElement.classList.remove('active');
    }

    show() {
        this.tooltipElement.classList.add('active');
    }

    constructor(
        private element: ElementRef
    ) {}

    ngOnInit() {
        console.log( this.tooltip );
        this.tooltipElement.className = 'tooltip';
        this.element.nativeElement.appendChild( this.tooltipElement );
        this.element.nativeElement.classList.add('tooltip-container');
    }

}
